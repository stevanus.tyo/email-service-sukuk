/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bsm.controller;

import com.id.bsm.model.Email;
import com.id.bsm.ws.EmailService;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.jws.WebService;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author macbook
 */
@WebService(endpointInterface = "com.id.bsm.ws.EmailService")
public class EmailServiceImpl implements EmailService {

    @Override
    public String sendEmailConfirmationLetter(Email e) {
        String result;
        try {
            String from = "bsmnotifsis@bsm.co.id";
            String subject = "Confirmation Letter SBSN";
            String header = "<h3>Mandiri Syariah - Info Transaksi Surat Berharga Syariah Negara</h3>";
            String greeting = "Assalamu'alaikum Wr.Wb,<br><br>Yth.Bapak/Ibu Nasabah Mandiri Syariah<br>";
            String footer = "<br>Disclaimer.<br>Jika Anda bukan penerima yang dituju, dengan ini diberitahukan bahwa setiap pengungkapan, menyalin, mendistribusikan atau mengambil tindakan apapun berdasarkan isi informasi ini sangat dilarang dan mungkin melanggar hukum. "
                    + "Jika ada kekeliruan dalam pesan ini, harap segera hubungi BSM Call 14040 atau (021) 2953 4040<br>";
            String body = header + "<br>" + greeting + "<br>" + "Terimakasih telah melakukan transaksi pemesanan Surat Berharga Syariah Negara (SBSN) Ritel melalui Mandiri Syariah.<br>"
                    + "Semoga dana pembelian SBSN Anda memberikan keberkahan untuk keluarga dan negeri.<br>"
                    + "Bersama ini kami sampaikan Bukti Konfirmasi Kepemilikan SBSN Ritel Anda.<br><br>"
                    + "<hr width=\"100%\"/>" + footer;

            // Set smtp properties
            Properties properties = new Properties();
            properties.put("mail.smtp.host", "10.250.48.151");
            properties.put("mail.smtp.port", "25");

            Session session = Session.getDefaultInstance(properties, null);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(e.getTo()));
            message.setSubject(subject);
            message.setSentDate(new Date());

            // Set the email body
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(body, "text/html");

            // Set the email attachment file
            MimeBodyPart attachmentPart = new MimeBodyPart();
            FileDataSource fileDataSource = new FileDataSource(e.getFilename()) {

                @Override
                public String getContentType() {
                    return "application/octet-stream";
                }

            };

            attachmentPart.setDataHandler(new DataHandler(fileDataSource));
            attachmentPart.setFileName(fileDataSource.getName());

            // Add all parts of the email to Multipart object
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            multipart.addBodyPart(attachmentPart);
            message.setContent(multipart);

            // Send email
            Transport.send(message);

            result = "Email telah terkirim";

        } catch (MessagingException m) {
            result = m.getMessage();
        }
        
        return result;

    }

    @Override
    public String sendEmailErrorDukcapil(Email e) {
        
        String result;
        
        try {
            
            String ktp = e.getNomorKtp();
            
            String from = "bsmnotifsis@bsm.co.id";
            String subject = ">>BSMNet - Info Transaksi Surat Berharha Syariah Negara<<";
            String header = "<h3>BSMNet - Info Transaksi Surat Berharga Syariah Negara</h3>";
            String greeting = "Assalamu'alaikum Wr.Wb,<br><br>Yth.Bapak/Ibu Nasabah Mandiri Syariah<br>";
            String footer = "<br>Disclaimer.<br>Jika Anda bukan penerima yang dituju, dengan ini diberitahukan bahwa setiap pengungkapan, menyalin, mendistribusikan atau mengambil tindakan apapun berdasarkan isi informasi ini sangat dilarang dan mungkin melanggar hukum. "
                    + "Jika ada kekeliruan dalam pesan ini, harap segera hubungi BSM Call 14040 atau (021) 2953 4040<br>";
            String body = header + "<br>" + greeting + "<br>" + "Nomor identitas anda " + ktp + "tidak ditemukan pada Dukcapil online, "
                    + "mohon lakukan pembaharuan data di kantor cabang Mandiri Syariah. Apabila telah sesuai mohon lakukan pembaharuan "
                    + "data di Dukcapil setempat. Lakukan registrasi ulang apabila Anda sudah melakukan pembaharuan data.<br><br>"
                    + "<hr width=\"100%\"/>" + footer;

            // Set smtp properties
            Properties properties = new Properties();
            properties.put("mail.smtp.host", "10.250.48.151");
            properties.put("mail.smtp.port", "25");
            
            Session session = Session.getDefaultInstance(properties, null);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(e.getTo()));
            message.setSubject(subject);
            message.setSentDate(new Date());
            
            // Set the email body
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(body, "text/html");
            
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messagePart);
            message.setContent(multipart);
            
            // Send email
            Transport.send(message);

            result = "Email telah terkirim";            
            
        } catch (MessagingException m) {
            
            result = m.getMessage();
        
        }
        
        return result;
        
    }

}
