/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bsm.model;

import java.io.Serializable;

/**
 *
 * @author macbook
 */
public class Email implements Serializable {
    
    private static final long serialVersionUID = -5577579081118070434L;
    
    private String to;
    private String filename;
    private String nomorktp;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getNomorKtp() {
        return nomorktp;
    }

    public void setNomorKtp(String nomorktp) {
        this.nomorktp = nomorktp;
    }
    
}
