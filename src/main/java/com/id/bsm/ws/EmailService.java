/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bsm.ws;

import com.id.bsm.model.Email;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author macbook
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface EmailService {
    
    @WebMethod
    public String sendEmailConfirmationLetter(Email e);
    
    @WebMethod
    public String sendEmailErrorDukcapil(Email e);
}
